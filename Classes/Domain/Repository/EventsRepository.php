<?php
namespace Events\Simpleevents\Domain\Repository;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Georgy George <georgy.ge@pitsolutions.com>, PITS
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Events
 */
class EventsRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
	/**
    * function to getLatestevents
    *
    * @param int $storagePid
    * return result
    */
    public function getLatestevents($storagePid)
    {   
    	$date = date('Y-m-d');
        $sql = "SELECT * FROM tx_simpleevents_domain_model_events WHERE event_start_date>='$date' OR event_end_date>='$date'";
    	return $this->createQuery()->statement($sql)->execute();
    	
	}

    /**
    * function to getSearchevents
    *
    * @param int $searchdate,$searchword
    * return result
    */
    public function getSearchevents($searchdate,$searchword)
    {   
        if (empty($searchdate) && empty($searchword)) {
            echo "Enter the fields to search";
        }
        elseif (!empty($searchdate) && empty($searchword)) {
            $sql = "SELECT * FROM tx_simpleevents_domain_model_events WHERE event_start_date='$searchdate' OR event_end_date='$searchdate'";
            
        } 
        elseif (empty($searchdate) && !empty($searchword)) {
            $sql = "SELECT * FROM tx_simpleevents_domain_model_events WHERE event_title LIKE '%$searchword%' OR event_description LIKE '%$searchword%' OR event_location LIKE '%$searchword%'";
            
        }
         else {
             $sql = "SELECT * FROM tx_simpleevents_domain_model_events WHERE 
             (event_start_date='$searchdate' OR event_end_date='$searchdate') AND uid IN (SELECT uid FROM tx_simpleevents_domain_model_events WHERE event_title LIKE '%$searchword%' OR event_description LIKE '%$searchword%' OR event_location LIKE '%$searchword%')";
        }

        return $this->createQuery()->statement($sql)->execute();
        
    }

    /**
    * function to getDayevents
    *
    * @param int $searchdate
    * return result
    */
    public function getDayevents($searchdate)
    {   
       $sql = "SELECT * FROM tx_simpleevents_domain_model_events WHERE event_start_date='$searchdate' OR event_end_date='$searchdate'";
       return $this->createQuery()->statement($sql)->execute(TRUE);
        
    }

     /**
    * function to getdaylist
    *
    * @param int $searchdate
    * return result
    */
    public function getdaylist($title)
    {   
       $sql = "SELECT * FROM tx_simpleevents_domain_model_events WHERE event_title='$title'";
       return $this->createQuery()->statement($sql)->execute(TRUE);
        
    }

    /**
    * function to getEventlist
    *
    * @param int $month_num
    * return result
    */
    public function getEventlist($month_num,$year)
    {
        
        /*$sql = "SELECT * FROM tx_simpleevents_domain_model_events WHERE 
        (event_start_date BETWEEN '$year-$month_num-01' AND'$year-$month_num-$lastday') OR (event_end_date BETWEEN '$year-$month_num-01' AND'$year-$month_num-$lastday')";*/
        $sql = "SELECT * FROM tx_simpleevents_domain_model_events WHERE event_start_date LIKE '$year-$month_num%' OR event_end_date LIKE '$year-$month_num%'";
        return $this->createQuery()->statement($sql)->execute(TRUE);
    }

    
}

