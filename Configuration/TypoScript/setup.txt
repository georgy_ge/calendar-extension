
plugin.tx_simpleevents_eventlisting {
	view {
		templateRootPaths.0 = {$plugin.tx_simpleevents_eventlisting.view.templateRootPath}
		partialRootPaths.0 = {$plugin.tx_simpleevents_eventlisting.view.partialRootPath}
		layoutRootPaths.0 = {$plugin.tx_simpleevents_eventlisting.view.layoutRootPath}
	}
	persistence {
		storagePid = {$plugin.tx_simpleevents_eventlisting.persistence.storagePid}
	}
}

plugin.tx_simpleevents._CSS_DEFAULT_STYLE (
	textarea.f3-form-error {
		background-color:#FF9F9F;
		border: 1px #FF0000 solid;
	}

	input.f3-form-error {
		background-color:#FF9F9F;
		border: 1px #FF0000 solid;
	}

	.tx-simpleevents table {
		border-collapse:separate;
		border-spacing:10px;
	}

	.tx-simpleevents table th {
		font-weight:bold;
	}

	.tx-simpleevents table td {
		vertical-align:top;
	}

	.typo3-messages .message-error {
		color:red;
	}

	.typo3-messages .message-ok {
		color:green;
	}

)
