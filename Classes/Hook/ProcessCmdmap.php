<?php
namespace Events\Simpleevents\Hook;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class ProcessCmdmap {
	/**
    * hook that is called when an element shall get deleted
    *
    * @param string $table the table of the record
    * @param integer $id the ID of the record
    * @param array $record The accordant database record
    * @param boolean $recordWasDeleted can be set so that other hooks or
    * @param DataHandler $tcemainObj reference to the main tcemain object
    * @return   void
    */
    function processCmdmap_preProcess($command, &$table, $id, $value, $dataHandler) {
        if ($command == 'delete' && $table == 'tt_content') {
            $sql =$GLOBALS['TYPO3_DB']->sql_query("SELECT uid,pid FROM tt_content WHERE FIND_IN_SET($id,records) AND deleted = 0 AND hidden = 0"); 
            //DebuggerUtility::var_dump($sql);
                    //exit();
            //$row = $GLOBALS['TYPO3_DB']->sql_num_rows($sql); 
            $idvalue = "";
            $pidvalue = array();
            $i=0;
            while($res = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($sql))
                {
                    $idvalue .= ','. $res['pid'];
                    $pidvalue[] = $res['pid'];
                    $pagevalues = implode(",", $pidvalue);
                    $i++;
                }
            //if(!empty($idvalue)) {
                if($i > 0) {
                    $message = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Messaging\FlashMessage::class,
                    'Count is '. $i,
                    'The element is referenced '. $pagevalues, // [optional] the header
                    \TYPO3\CMS\Core\Messaging\FlashMessage::ERROR, // [optional] the severity defaults to \TYPO3\CMS\Core\Messaging\FlashMessage::OK
                    true // [optional] whether the message should be stored in the session or only in the \TYPO3\CMS\Core\Messaging\FlashMessageQueue object (default is false)
                );
                $flashMessageService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Messaging\FlashMessageService::class);
                $messageQueue = $flashMessageService->getMessageQueueByIdentifier();
                $messageQueue->addMessage($message);
                $table = "";
                }
        }
    }
}

