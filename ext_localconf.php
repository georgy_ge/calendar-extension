<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Events.' . $_EXTKEY,
	'Eventlisting',
	array(
		'Events' => 'list, search, month, show, day ',
		
	),
	// non-cacheable actions
	array(
		'Events' => 'list, search, month, show, day ',
		
	)
);

$GLOBALS ['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processCmdmapClass'][] = 'Events\\Simpleevents\\Hook\\ProcessCmdmap';
