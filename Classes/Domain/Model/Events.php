<?php
namespace Events\Simpleevents\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Georgy George <georgy.ge@pitsolutions.com>, PITS
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Events
 */
class Events extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * eventTitle
     *
     * @var string
     * @validate NotEmpty
     */
    protected $eventTitle = '';
    
    /**
     * eventURL
     *
     * @var string
     */
    protected $eventURL = '';
    
    /**
     * eventDescription
     *
     * @var string
     */
    protected $eventDescription = '';
    
    /**
     * eventLocation
     *
     * @var string
     * @validate NotEmpty
     */
    protected $eventLocation = '';
    
    /**
     * eventStartDate
     *
     * @var \DateTime
     * @validate NotEmpty
     */
    protected $eventStartDate = null;
    
    /**
     * eventEndDate
     *
     * @var \DateTime
     * @validate NotEmpty
     */
    protected $eventEndDate = null;
    
    /**
     * starttimeofevent
     *
     * @var string
     */
    protected $starttimeofevent = '';
    
    /**
     * imageUpload
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $imageUpload = null;
    
    /**
     * Returns the eventTitle
     *
     * @return string $eventTitle
     */
    public function getEventTitle()
    {
        return $this->eventTitle;
    }
    
    /**
     * Sets the eventTitle
     *
     * @param string $eventTitle
     * @return void
     */
    public function setEventTitle($eventTitle)
    {
        $this->eventTitle = $eventTitle;
    }
    
    /**
     * Returns the eventURL
     *
     * @return string $eventURL
     */
    public function getEventURL()
    {
        return $this->eventURL;
    }
    
    /**
     * Sets the eventURL
     *
     * @param string $eventURL
     * @return void
     */
    public function setEventURL($eventURL)
    {
        $this->eventURL = $eventURL;
    }
    
    /**
     * Returns the eventDescription
     *
     * @return string $eventDescription
     */
    public function getEventDescription()
    {
        return $this->eventDescription;
    }
    
    /**
     * Sets the eventDescription
     *
     * @param string $eventDescription
     * @return void
     */
    public function setEventDescription($eventDescription)
    {
        $this->eventDescription = $eventDescription;
    }
    
    /**
     * Returns the eventLocation
     *
     * @return string $eventLocation
     */
    public function getEventLocation()
    {
        return $this->eventLocation;
    }
    
    /**
     * Sets the eventLocation
     *
     * @param string $eventLocation
     * @return void
     */
    public function setEventLocation($eventLocation)
    {
        $this->eventLocation = $eventLocation;
    }
    
    /**
     * Returns the eventStartDate
     *
     * @return \DateTime $eventStartDate
     */
    public function getEventStartDate()
    {
        return $this->eventStartDate;
    }
    
    /**
     * Sets the eventStartDate
     *
     * @param \DateTime $eventStartDate
     * @return void
     */
    public function setEventStartDate(\DateTime $eventStartDate)
    {
        $this->eventStartDate = $eventStartDate;
    }
    
    /**
     * Returns the eventEndDate
     *
     * @return \DateTime $eventEndDate
     */
    public function getEventEndDate()
    {
        return $this->eventEndDate;
    }
    
    /**
     * Sets the eventEndDate
     *
     * @param \DateTime $eventEndDate
     * @return void
     */
    public function setEventEndDate(\DateTime $eventEndDate)
    {
        $this->eventEndDate = $eventEndDate;
    }
    
    /**
     * Returns the imageUpload
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $imageUpload
     */
    public function getImageUpload()
    {
        return $this->imageUpload;
    }
    
    /**
     * Sets the imageUpload
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $imageUpload
     * @return void
     */
    public function setImageUpload(\TYPO3\CMS\Extbase\Domain\Model\FileReference $imageUpload)
    {
        $this->imageUpload = $imageUpload;
    }
    
    /**
     * Returns the starttimeofevent
     *
     * @return string $starttimeofevent
     */
    public function getStarttimeofevent()
    {
        return $this->starttimeofevent;
    }
    
    /**
     * Sets the starttimeofevent
     *
     * @param string $starttimeofevent
     * @return void
     */
    public function setStarttimeofevent($starttimeofevent)
    {
        $this->starttimeofevent = $starttimeofevent;
    }

}