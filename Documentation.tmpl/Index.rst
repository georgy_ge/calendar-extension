﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt

.. _start:

=============================================================
Simple Event Extension
=============================================================

.. only:: html

	:Classification:
		simpleevents

	:Version:
		|release|

	:Language:
		en

	:Description:
		This simple event extension displays the list of events.

	:Keywords:
		comma,separated,list,of,keywords

	:Copyright:
		2017

	:Author:
		Georgy George

	:Email:
		georgy.ge@pitsolutions.com

	:License:
		This document is published under the Open Content License
		available from http://www.opencontent.org/opl.shtml

	:Rendered:
		|today|

	The content of this document is related to TYPO3,
	a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.

	**Table of Contents**

.. toctree::
	:maxdepth: 3
	:titlesonly:

	Introduction/Index
	User/Index
	Administrator/Index
	Configuration/Index
	Developer/Index
	KnownProblems/Index
	ToDoList/Index
	ChangeLog/Index
	Links
