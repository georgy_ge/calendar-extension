<?php

namespace Events\Simpleevents\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Georgy George <georgy.ge@pitsolutions.com>, PITS
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \Events\Simpleevents\Domain\Model\Events.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Georgy George <georgy.ge@pitsolutions.com>
 */
class EventsTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \Events\Simpleevents\Domain\Model\Events
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \Events\Simpleevents\Domain\Model\Events();
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getEventTitleReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getEventTitle()
		);
	}

	/**
	 * @test
	 */
	public function setEventTitleForStringSetsEventTitle()
	{
		$this->subject->setEventTitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'eventTitle',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getEventURLReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getEventURL()
		);
	}

	/**
	 * @test
	 */
	public function setEventURLForStringSetsEventURL()
	{
		$this->subject->setEventURL('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'eventURL',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getEventDescriptionReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getEventDescription()
		);
	}

	/**
	 * @test
	 */
	public function setEventDescriptionForStringSetsEventDescription()
	{
		$this->subject->setEventDescription('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'eventDescription',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getEventLocationReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getEventLocation()
		);
	}

	/**
	 * @test
	 */
	public function setEventLocationForStringSetsEventLocation()
	{
		$this->subject->setEventLocation('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'eventLocation',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getEventStartDateReturnsInitialValueForDateTime()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getEventStartDate()
		);
	}

	/**
	 * @test
	 */
	public function setEventStartDateForDateTimeSetsEventStartDate()
	{
		$dateTimeFixture = new \DateTime();
		$this->subject->setEventStartDate($dateTimeFixture);

		$this->assertAttributeEquals(
			$dateTimeFixture,
			'eventStartDate',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getEventEndDateReturnsInitialValueForDateTime()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getEventEndDate()
		);
	}

	/**
	 * @test
	 */
	public function setEventEndDateForDateTimeSetsEventEndDate()
	{
		$dateTimeFixture = new \DateTime();
		$this->subject->setEventEndDate($dateTimeFixture);

		$this->assertAttributeEquals(
			$dateTimeFixture,
			'eventEndDate',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getStarttimeofeventReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getStarttimeofevent()
		);
	}

	/**
	 * @test
	 */
	public function setStarttimeofeventForStringSetsStarttimeofevent()
	{
		$this->subject->setStarttimeofevent('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'starttimeofevent',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getImageUploadReturnsInitialValueForFileReference()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getImageUpload()
		);
	}

	/**
	 * @test
	 */
	public function setImageUploadForFileReferenceSetsImageUpload()
	{
		$fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$this->subject->setImageUpload($fileReferenceFixture);

		$this->assertAttributeEquals(
			$fileReferenceFixture,
			'imageUpload',
			$this->subject
		);
	}
}
