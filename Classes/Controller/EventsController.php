<?php
namespace Events\Simpleevents\Controller;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Georgy George <georgy.ge@pitsolutions.com>, PITS
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * EventsController
 */
class EventsController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * eventsRepository
     *
     * @var \Events\Simpleevents\Domain\Repository\EventsRepository
     * @inject
     */
    protected $eventsRepository = NULL;
    
    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $arguments = $this->request->getArguments();
        $searchdate = $arguments['newPost']['dateRange']['minimumValue'];
        //$enddate = $arguments['newPost']['dateRange']['maximumValue'];
        $searchword = $arguments['newPost']['keyword'];
        $submit = $arguments['newPost']['submit'];
        $this->contentObj = $this->configurationManager->getContentObject();
        $storagePid = $this->contentObj->data['pages'];
        $events = $this->eventsRepository->getLatestevents($storagePid);
        $this->view->assign('events', $events);
    }

    /**
     * function for search action
     *
     * @return void
     */
    public function searchAction() {
        $arguments = $this->request->getArguments();
        $searchdate = $arguments['newPost']['dateRange']['minimumValue'];
        //$enddate = $arguments['newPost']['dateRange']['maximumValue'];
        $searchword = $arguments['newPost']['keyword'];
        $searchvalue = $this->eventsRepository->getSearchevents($searchdate,$searchword);
        $this->view->assign('searchevents', $searchvalue);       
    }

    /**
     * function for month action
     *
     * @return void
     */
    public function monthAction() {
        $arguments = $this->request->getArguments();
        if (isset($arguments['month_num'])) {
            $month_num = $arguments['month_num'];
            $year = $arguments['year'];
        }
        else {
            $day_num=date("j"); //Today's date
            $month_num = date("m"); // The month
            $year = date("Y"); //The year
        }
        //DebuggerUtility::var_dump($month_num);

        $date_today = getdate(mktime(0,0,0,$month_num,1,$year)); //Returns array of date information for the first day of the month eg: for march, about 1st march
        $month_name = $date_today["month"]; // The current month name
        $first_week_day = $date_today["wday"]; //   Numeric representation of the day of the week->0 for sunday

        // Find the final day of the month
        $cont = true;
        $today = 27; // The last day of the month is grater than 27
        while (($today <= 32) && ($cont)) {
            $date_today = getdate(mktime(0,0,0,$month_num,$today,$year));
            if ($date_today["mon"] != $month_num) {
                $lastday = $today - 1;
                $cont = false;
            }
            $today++; 
        }
        
        $day = 1;
        $wday = $first_week_day;
        $firstweek = "true";
        $arraylist = array();
        $j = 0;
        if ($firstweek) {
            for ($i=1; $i <= $first_week_day ; $i++) { 
                $arraylist[$j] = 0;
                $j++;
            }
        }

        // New arraylist to display date
        $date_from = "$year-$month_num-01";
        $date_from = strtotime($date_from);


        $date_to = "$year-$month_num-$lastday";
        $date_to = strtotime($date_to);
        
        //Code to get the full days of a month eg:1-31
        for ($day= $date_from; $day <= $date_to; $day+= 86400) {  
           // $$arraylist[$j] = date("d", $day);
            $arraylist[$j] = date("Y-m-d", $day); 
            $j++; 
        }
        //DebuggerUtility::var_dump($arraylist);


        //Giving zero values for the beginning of a date
        $day = 1;
        $wday = $first_week_day;
        $firstweek = "true";
        $testarray = array();
        $q=0;
        if ($firstweek) {
            for ($i=1; $i <= $first_week_day ; $i++) { 
                $testarray[$q] = 0;
                $q++;
            }
        }        
        
        //Getting event title, date, url of the events of the respective month
        $eventarray = array();
        $z =0;
        //$testarray = array();
        $eventlists = $this->eventsRepository->getEventlist($month_num,$year);
        //DebuggerUtility::var_dump($eventlists);

        foreach ($eventlists as $key => $value) {
            $startdate = $value['event_start_date'];
            $startdate = strtotime($startdate);
            $enddate = $value['event_end_date'];
            $enddate = strtotime($enddate);
            for ($day1= $startdate; $day1 <= $enddate; $day1+= 86400) {
                //$eventarray[$z] = date("Y-m-d", $day1);
                $eventarray[$z] = array ('date' => date("Y-m-d", $day1) , 'title' => $value['event_title'] , 'url' => $value['event_u_r_l']);
                $z++;
            }
        }
        //DebuggerUtility::var_dump($eventarray);


        $event_count = count($eventarray );
        for ($day= $date_from; $day <= $date_to; $day+= 86400) {  

            $cal_date = date("Y-m-d", $day);
            $cal_day['date'] = date("Y-m-d", $day);
            $cal_day['title'] = '';
            $cal_day['url'] = '';
            $title_array = array();
            //$url_array = array();
            //$z =0;
            for ($z=0; $z<$event_count; $z++) {
               // $eventarray[$z] = date("Y-m-d", $day1);
                if ($cal_date == $eventarray[$z]['date']) {
                   array_push($title_array,$eventarray[$z]['title']);
                   //DebuggerUtility::var_dump($title_array);
                   //$testarray[$q] = array (date("Y-m-d", $day),$title_array);
                   $cal_day['title'] = $title_array;
                   //array_push($url_array,$eventarray[$z]['url']);
                   //$cal_day['url'] = $url_array;
                   //array_push($title_array,$eventarray[$z]['url']);
                   //$cal_day['url'] = $title_array;
                   //DebuggerUtility::var_dump($cal_day);
               }

              }
              //DebuggerUtility::var_dump($cal_day);
               array_push($testarray, $cal_day);
               $q++;
        }

        //DebuggerUtility::var_dump($testarray);

        $this->view->assign('eventlist', $eventlists);
        $this->view->assign('testlists', $testarray);
        //$eventnew = (array) $eventlists;
        
        $this->view->assign('arraylist', $arraylist);

        // Code for previous link
        if ($month_num == 01) {
            $this->view->assign('prevParams', array('month_num' => 12, 'year' => $year - 1, 'testlists' => $testarray));
        }
        else {
            $prevmonth = $month_num - 1;
            $prevmonthpad = str_pad($prevmonth, 2, "0", STR_PAD_LEFT);
            $this->view->assign('prevParams', array('month_num' => $prevmonthpad, 'year' => $year));
        }

        //Code for next link
        if($month_num == 12) {
            $this->view->assign('nextParams', array('month_num' => 01, 'year' => $year + 1));
        } else {
            $nextmonth = $month_num + 1;
            $nextmonthpad = str_pad($nextmonth, 2, "0", STR_PAD_LEFT);
            $this->view->assign('nextParams', array('month_num' => $nextmonthpad, 'year' => $year));
        }

        //$arraylist = array($day_num,$month_num);
        $daylists = array('day_num' => $day_num, 'month_num' => $month_num, 'year' => $year, 'month_name' => $month_name, 'first_week_day' => $first_week_day, 'lastday' => $lastday);
        //DebuggerUtility::var_dump($daylists);
        $this->view->assign('daylists', $daylists);
    }

    /**
     * function for day action
     *
     * @return void
     */
    public function dayAction() {
        $arguments = $this->request->getArguments();
        $searchdate = $arguments['newPost']['dateRange']['minimumValue'];
        if (empty($searchdate)) {
            echo "Enter the date to be viewed here";
        }
        else {
            $sepvalues = explode("-", $searchdate);
            $year = $sepvalues[0];
            $month_num = $sepvalues[1];
            $eventlists = $this->eventsRepository->getEventlist($month_num,$year);
            $eventarray = array();
            $z = 0;
            foreach ($eventlists as $key => $value) {
                $startdate = $value['event_start_date'];
                $startdate = strtotime($startdate);
                $enddate = $value['event_end_date'];
                $enddate = strtotime($enddate);
                
                for ($days=$startdate; $days <= $enddate; $days+= 86400) { 
                    $eventarray[$z] = array ('date' => date("Y-m-d", $day1) , 'title' => $value['event_title'] , 'url' => $value['event_u_r_l']);
                    $z++;
                }
            }
            //DebuggerUtility::var_dump($eventlists);

            //DebuggerUtility::var_dump($month_num);
            $searchvalue = $this->eventsRepository->getDayevents($searchdate);
            
            $arraytest = array();
            $k =0;
            /*foreach ($searchvalue as $key => $value) {
                for ($day= $date_from = strtotime($value['event_start_date']); $day<=strtotime($value['event_end_date']); $day+= 86400) { 
                    $arraytest[$k] = array('date' => date("Y-m-d", $day) , 'title' => $value['event_title']);
                    $title = $arraytest[$k]['title'];
                    //DebuggerUtility::var_dump($title);
                    if ($searchdate == $arraytest[$k]['date']) {
                        $searchvaluenew = $this->eventsRepository->getdaylist($title);
                        //DebuggerUtility::var_dump($searchvaluenew);
                        $this->view->assign('searchevents', $searchvaluenew);
                    }
                    $k++; 
                }
            }*/ 
        }
    }
    
    
    /**
     * action show
     *
     * @param \Events\Simpleevents\Domain\Model\Events $events
     * @return void
     */
    public function showAction(\Events\Simpleevents\Domain\Model\Events $events)
    {
        $this->view->assign('events', $events);
    }

}